from django.apps import AppConfig


class DjangocmsPaiBootstrap3Config(AppConfig):
    name = 'djangocms_pai_bootstrap3'
